import { imageUrlToBase64 } from './image-conversion';


/** @function initHtmlStr - Initialize the output HTML. */
export function initHtmlStr(url: string): string {
	return `<!DOCTYPE html>
		<html lang="en">
			<head>
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0" />
				<title>Image Table | ${url}</title>
				<style>
					.flex-table {
						display: flex;
						flex-direction: column;
						justify-content: space-between;
						align-items: strech;
						align-content: center;
					}
					.flex-table-row {
						display: flex;
						flex-direction: row;
						justify-content: space-between;
						align-items: strech;
						align-content: center;
						border: 1px solid black;
					}
					.flex-table-cell {}
					img {
						max-height: 120px;
						max-width: 120px;
						height: auto;
					}
					.padded { padding: 10px; }
				</style>
			</head>
			<body>
				<div class="flex-table">`;
}
/** @function addToHtmlStr - Add an image row to the main table. */
export function addToHtmlStr(imgUrl: string, imgSrc: string): string {
	return `<div class="flex-table-row">
		<p class="flex-table-cell padded">${imgUrl}</p>
		<img class="flex-table-cell padded" src="${imgSrc}" />
	</div>`;
}
/** @function finalizeHtmlStr - Finalize the output HTML. */
export function finalizeHtmlStr(): string {
	return `</div>
		</body>
	</html>`;
}

/** @function createHtmlStr - uses initHtmlStr, addToHtmlStr & finalizeHtmlStr
 * to output the final HTML code.
 */
export async function createHtmlStr(
	mainUrl: string, imgSrcArr: string[], saveImages: boolean = false
): Promise<string> {
	let htmlStrOut = initHtmlStr(mainUrl);
	let i = imgSrcArr.length;
	while(i--) {
		if(!imgSrcArr[i]) continue;
		const imageSrc = (saveImages)?
			await imageUrlToBase64(imgSrcArr[i]) : imgSrcArr[i];
		htmlStrOut += addToHtmlStr(imgSrcArr[i], imageSrc);
	}
	return htmlStrOut + finalizeHtmlStr();
}
