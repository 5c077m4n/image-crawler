import { requestImage } from './requests';


export function imageUrlToBase64(imageUrl: string): Promise<any> {
	return requestImage(imageUrl)
		.then(async (image: any) => {
			if (!image) return;
			return await Promise.resolve(Buffer.from(image))
				.catch((err: Error) => {
					process.stdout.write('\r');
					console.error(`[Image Compression] ${err}`);
				});
		})
		.then((imgBuf: void | Buffer) => (imgBuf) ? imgBuf.toString('base64') : undefined)
		.then((imgStr: string | void) => (imgStr) ? 'data:image/*;charset=utf-8;base64, ' + imgStr : undefined);
}
