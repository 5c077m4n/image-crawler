/** @class ProgressSpinner - a CLI spinner to pass the time */
export class ProgressSpinner {
	private intervalId: any;
	private patters: string[];
	constructor() {
		this.patters = ['\\', '|', '/', '-'];
	}

	public start(): void {
		let x = 0;
		this.intervalId = setInterval(() => {
			process.stdout.write(`\r ${this.patters[x++]} `);
			x %= this.patters.length;
		}, 500);
	}
	public stop(): void {
		process.stdout.write('\r');
		clearInterval(this.intervalId);
	}
}
