import request from 'request-promise';


export function requestHtml(url: string): Promise<string> {
	return request(url, { resolveWithFullResponse: false })
		.catch((err: Error) => {
			process.stdout.write('\r');
			console.error(`[HTML Request] ${err}`);
			process.exit(-1);
		});
}

export function requestImage(url: string): Promise<string | undefined> {
	return request(url)
		.catch((err: Error) => {
			process.stdout.write('\r');
			console.error(`[Image Request] ${err}`);
		});
}
