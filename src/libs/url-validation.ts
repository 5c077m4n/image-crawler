/** @function normalizeUrl - Lowercase the URL and add "http://" if needed. */
export function normalizeUrl(url: string): string {
	if(!url) {
		process.stdout.write('\r');
		console.error('Please insert a valid URL.');
		process.exit(-1);
	}
	url = url.toLowerCase();
	return (/^http/g).test(url)? url : `http://${url}`;
}

/** @function normalizeImgSrc - Normalizes the image URL. */
export function normalizeImgSrc(srcUrl: string, mainUrl: string): string {
	if((/^\//g).test(srcUrl)) {
		if((/^\/\//g).test(srcUrl)) return `http:${srcUrl}`;
		else return normalizeUrl(mainUrl) + srcUrl;
	}
	else return srcUrl;
}
