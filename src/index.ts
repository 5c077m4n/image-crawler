import Bluebird from 'bluebird';
global.Promise = Bluebird;
import program from 'commander';
import { ensureFile, writeFile } from 'fs-extra';
import { load } from 'cheerio';

import { normalizeUrl, normalizeImgSrc } from './libs/url-validation';
import { createHtmlStr } from './libs/html-functions';
import { ProgressSpinner } from './libs/progress-spinner';
import { requestHtml, requestImage } from './libs/requests';


console.time('Completed in');

const spinner = new ProgressSpinner();

program
	.option('-u, --url <string>', 'Choose which URL you want to crawl')
	.option('-o, --output-file [string]', 'Write the output file\'s name', './out/image-table.html')
	.option('-m, --minify-file [boolean]', 'Minify the output HTML file', false)
	.option('-s, --save-images [boolean]', 'Choose whether to save the images locally or not', false)
	.parse(process.argv);

/** @async @function - the main pormise */
ensureFile(program.outputFile)
	.then(() => normalizeUrl(program.url))
	.tap(() => console.log('Gathering requested images, just a minute...'))
	.tap(() => spinner.start())
	.then(async (url: string) => await requestHtml(url))
	.then(load)
	.then(($: Function) => $('img').get())
	.map((img: any) => img.attribs.src)
	.map((imgSrc: string) => normalizeImgSrc(imgSrc, program.url))
	.then((imageUrlList: string[]) => createHtmlStr(
		program.url, imageUrlList, program.saveImages
	))
	.then(async (htmlStr: string) => await writeFile(program.outputFile, htmlStr))
	.tap(() => spinner.stop())
	.tap(() => console.log(`"${program.outputFile}" is now ready.`))
	.catch((err: Error) => {
		spinner.stop();
		console.error(`[File System] ${err}`);
		console.timeEnd('Completed in');
		process.exit(-1);
	})
	.tap(() => console.timeEnd('Completed in'));
